+++
title = "F.A.Q. (Frequently Asked Questions)"
description = "FAQ atau Pertanyaan yang biasa ditanyakan"
date = "2018-03-30T10:34:07+07:00"
slug = "faq"
layout = "tentang-kami"
+++

#### Apakah melayani COD (Cash On Delivery), ketemuan, pembayaran setelah barang diterima, atau bayar DP dahulu?
**Jawab:** Tidak, kami hanya melayani transaksi secara online di mana barang akan dikirim setelah pembayaran kami terima secara penuh.

#### Apakah bisa beli/ambil barang langsung di toko?
**Jawab:** Tidak, kami hanya melayani pembelian secara online via Website, Email, SMS, Chat, maupun Sosial Media.

#### Barang dikirim menggunakan ekspedisi apa?
**Jawab:** Kami biasa menggunakan jasa pengiriman barang JNE, namun apabila Anda menginginkan pengiriman menggunakan jasa pengiriman lain seperti Pos Indonesia, Wahana, dll silahkan hubungi Customer Service kami.

#### Kapan barang saya dikirim?
**Jawab:** Apabila pembayaran Anda kami terima sebelum pukul 14:30 maka barang akan dikirim pada hari yang sama dengan syarat produk sedang ready stock, kecuali hari Sabtu, Minggu, dan Libur. Apabila pembayaran kami terima lebih dari jam 14:30 atau pada hari libur, maka barang akan dikirim pada hari kerja selanjutnya.

#### Mengapa saya belum menerima kode resi pengiriman?
**Jawab:** Kode Resi Pengiriman akan diberikan setelah kami menerima resi dari pihak jasa pengiriman, biasanya paling lambat 1×24 jam kami akan berikan kode resi kepada Anda.

#### Kode resi pengiriman tidak bisa dicek/tracking?
**Jawab:** Apabila kode resi pengiriman masih baru memang biasanya tidak bisa langsung ditracking karena sistem dari jasa pengiriman butuh waktu untuk melakukan update data, jadi mohon bersabar.

#### Apakah saya bisa jadi reseller?
**Jawab:** Tentu saja bisa, kami sangat memprioritaskan para reseller. Untuk informasi tentang reseller silahkan hubungi Customer Service kami.

#### Bagaimana dengan kualitas barang?
**Jawab:** Barang yang kami jual sudah pasti berkualitas dan dijamin asli. Kami juga selalu cek kondisi barang sebelum dikirim untuk memastikan barang dalam kondisi terbaik saat dikirim.

Namun jika Anda masih mendapat produk yang catat, label yang ada pada jaket Hijacket jangan sampai dilepas. kemudian silakan kirim rekaman video durasi minimal 30 detik pada tempat yang catat tersebut kepada kami, kami akan ganti produknya dengan ongkos kirim kami yang tanggung.

#### Packing/bungkus barang yang dikirim seperti apa?
**Jawab:** Barang yang dikirim akan kami bungkus dengan rapat, rapi, dan tidak menocol.

#### Apakah kerahasiaan data pembeli terjamin?
**Jawab:** Kami sangat menghormati dan menjaga data pribadi Anda. Kami tidak akan mempublikasikan Nama, Alamat, No.HP, maupun informasi pribadi Anda secara sembarangan.

#### Apakah alamat kirim harus lengkap?
**Jawab:** Ya, sebaiknya Anda memberikan alamat selengkap-lengkapnya termasuk seperti kelurahan, kecamatan, kota, provinsi dan kode pos. Apabila diperlukan beri keterangan tambahan patokan alamat Anda supaya kurir dapat lebih jelas saat mengirim.

#### Mengapa barang saya masih belum sampai?
**Jawab:** Lama pengiriman barang berbeda-beda, tergantung jarak kota dan alamat Anda. Biasanya untuk kota-kota besar paling lama sekitar 2-3 hari, namun tidak menutup kemungkinan bisa lebih cepat.

#### Mengapa barang saya masih belum sampai?
**Jawab:** Anda bisa cek status dan posisi barang Anda dengan menggunakan kode resi pengiriman pada website dari jasa pengiriman, atau hubungi Call Center dari jasa pengiriman terkait.

#### Apakah saya bisa memesan barang yang lain?
**Jawab:** Bisa, kami akan berusaha menyediakan barang yang Anda pesan, silahkan hubungi Customer Service kami.