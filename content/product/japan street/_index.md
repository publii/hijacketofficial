---
title: Hijacket Japan Street Original
description: "Hijacket Japan Street Original terinspirasi dari negeri sakura. Buat kamu yang suka Jepang, Hijacket Japan Street dengan sablon 旅行者(Traveler) siap menemani perjalanan & gaya kamu ke negeri sakura." 
image: "/images/hijacket-japan-street-cover.png"
slug: "japanstreet"
---

Hijacket Japan Street Original terinspirasi dari negeri sakura. Buat kamu yang suka Jepang, Hijacket Japan Street dengan sablon 旅行者(Traveler) siap menemani perjalanan & gaya kamu ke negeri sakura. Yuk miliki sekarang juga warna favoritmu ! 